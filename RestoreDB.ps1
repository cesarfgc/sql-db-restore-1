clear

# S3 Bucket & Files
$bucketName = "vk-ptp"
$bakupFileName = "PSRestoreTest.bak"
$scriptFfile = "Insert.sql"

# Local OS
$hostname = (hostname)

# Local File System
$rootPath = "C:\Squirt\"
$backupPath = "$rootPath\Backup\"
$logPath ="$rootPath\Logs\"
$dataPath ="$rootPath\Data\"
$scriptPath ="$rootPath\Scripts\"

# Sql Server
$sqlServerName = $hostname
$sqlLogin="sa"
$sqlPassword="52CXIQ!17o6*3nOt"
$username = "squirt-sql-user"
$password = "VRM8i6H8RCByezk5YDLm"

# Script
$outputFile = "C:\users\administrator\Documents\output.json"

#########################################################################################
# Create folders
#########################################################################################
mkdir $rootPath
mkdir $backupPath
mkdir $logPath
mkdir $dataPath
mkdir $scriptPath
#########################################################################################

#########################################################################################
# Download from S3
#########################################################################################
Copy-S3Object -BucketName $bucketName -key $bakupFileName -file $backupPath\$bakupFileName
#Copy-S3Object -BucketName $bucketName -key bakupFileName -file $backupPath\$scriptFfile
#########################################################################################

<#
#########################################################################################
# Grant sql credentials to local administrator
#########################################################################################
$sqlCmd = "create login [$hostname\Administrator] from windows;"
SQLCMD -S $SqlServerName -Q $sql -U $sqlLogin -P $sqlPassword

$sql = "ALTER SERVER ROLE [sysadmin] ADD MEMBER [$hostname\Administrator];"
SQLCMD -S $SqlServerName -Q $sql -U $sqlLogin -P $sqlPassword
#########################################################################################
#>

#########################################################################################
# Restore database
#########################################################################################
$results = SQLCMD -E -S $SqlServerName -Q "restore headeronly from disk = '$($backupPath+$bakupFileName)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.databasename -ne $null}		

$DBName = $results.databasename

$results = SQLCMD -E -S $SqlServerName -Q "restore filelistonly from disk = '$($backupPath+$bakupFileName)'" -W -m 1 -s "|"|convertfrom-csv -Delimiter "|"|Select-Object -Skip 1|where{$_.type -ne $null}		
           
foreach($result in $results) {
  switch($result.type){
    D {$dataname = $result.LogicalName;$dataphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
    L {$Logname = $result.LogicalName;$logphname = $result.PhysicalName.replace((split-path $result.PhysicalName),"").replace("\","") }
  }
}   

$RestoreQuery = "RESTORE DATABASE [$DBName] FROM DISK = '$($backupPath+$bakupFileName)' WITH MOVE "
$RestoreQuery +="'$($dataname)' TO '$($dataPath+$dataphname)', MOVE '$($Logname)' TO '$($dataPath+$logphname)', RECOVERY, REPLACE, STATS = 10;"
Write-host "Running restore query: $($RestoreQuery)"
$restore = SQLCMD -E -S $SqlServerName -Q $RestoreQuery -W                
if(($restore|select -last 1|where{$_ -match "successfully"}).count -eq 0 ){
    write-host "Restore failed" -ForegroundColor red
    $restore
}else{
    write-host "Restore succedded" -ForegroundColor green
    $restore|select -last 1
}
#########################################################################################

<#
Write-host "Running script..."
$scriptrun = SQLCMD -E -S $SqlServerName -i $($dest+$scriptfile) -W -b                
if($lastexitcode -eq 0 ){
    write-host "Script succeeded" -ForegroundColor green
}else{
    write-host "Script failed" -ForegroundColor red
}
#>

$scriptrun   

#########################################################################################
# Create application user
#########################################################################################
sqlcmd -S $SqlServerName -Q "use [$DBName]; CREATE LOGIN $username WITH PASSWORD = '$($password)';CREATE USER $username FOR LOGIN $username;EXEC sp_addrolemember N'db_owner', N'$($username)';"

if($lastexitcode -eq 0 ){
    write-host "Create user succeeded" -ForegroundColor green
}else{
    write-host "Create user failed" -ForegroundColor red
}
$instance = (Invoke-WebRequest http://169.254.169.254/latest/dynamic/instance-identity/document -useBasicParsing -ErrorAction SilentlyContinue).content|convertfrom-json
$string=@"
{
  "connectionString": 
  "Provider=SQLNCLI11;Server=$($instance.privateip);Database=$DBname;Uid=$username;Pwd=$password;"
}
"@
#########################################################################################
set-content $outputfile -value $string -force